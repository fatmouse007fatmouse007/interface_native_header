/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 /**
 * @file task.h
 *
 * @brief Declares the task interfaces in C++.
 *
 * @since 10
 * @version 1.0
 */
#ifndef FFRT_API_CPP_TASK_H
#define FFRT_API_CPP_TASK_H
#include <vector>
#include <string>
#include <functional>
#include <memory>
#include "c/task.h"

namespace ffrt {
class task_attr : public ffrt_task_attr_t {
public:
    task_attr()
    {
        ffrt_task_attr_init(this);
    }

    ~task_attr()
    {
        ffrt_task_attr_destroy(this);
    }

    task_attr(const task_attr&) = delete;
    task_attr& operator=(const task_attr&) = delete;

    /**
     * @brief Sets a task name.
     *
     * @param name Indicates a pointer to the task name.
     * @since 10
     * @version 1.0
     */
    inline task_attr& name(const char* name)
    {
        ffrt_task_attr_set_name(this, name);
        return *this;
    }

    /**
     * @brief Obtains the task name.
     *
     * @return Returns a pointer to the task name.
     * @since 10
     * @version 1.0
     */
    inline const char* name() const
    {
        return ffrt_task_attr_get_name(this);
    }

    /**
     * @brief Sets the QoS for this task.
     *
     * @param qos Indicates the QoS.
     * @since 10
     * @version 1.0
     */
    inline task_attr& qos(enum qos qos)
    {
        ffrt_task_attr_set_qos(this, static_cast<ffrt_qos_t>(qos));
        return *this;
    }

    /**
     * @brief Obtains the QoS of this task.
     *
     * @return Returns the QoS.
     * @since 10
     * @version 1.0
     */
    inline enum qos qos() const
    {
        return static_cast<enum qos>(ffrt_task_attr_get_qos(this));
    }

    /**
     * @brief Sets the delay time for this task.
     *
     * @param delay_us Indicates the delay time, in microseconds.
     * @since 10
     * @version 1.0
     */
    inline task_attr& delay(uint64_t delay_us)
    {
        ffrt_task_attr_set_delay(this, delay_us);
        return *this;
    }

    /**
     * @brief Obtains the delay time of this task.
     *
     * @return Returns the delay time.
     * @since 10
     * @version 1.0
     */
    inline uint64_t delay() const
    {
        return ffrt_task_attr_get_delay(this);
    }
};

class task_handle {
public:
    task_handle() : p(nullptr)
    {
    }
    task_handle(ffrt_task_handle_t p) : p(p)
    {
    }

    ~task_handle()
    {
        if (p) {
            ffrt_task_handle_destroy(p);
        }
    }

    task_handle(task_handle const&) = delete;
    void operator=(task_handle const&) = delete;

    inline task_handle(task_handle&& h)
    {
        *this = std::move(h);
    }

    inline task_handle& operator=(task_handle&& h)
    {
        if (p) {
            ffrt_task_handle_destroy(p);
        }
        p = h.p;
        h.p = nullptr;
        return *this;
    }

    inline operator void* () const
    {
        return p;
    }

private:
    ffrt_task_handle_t p = nullptr;
};

template<class T>
struct function {
    template<class CT>
    function(ffrt_function_header_t h, CT&& c) : header(h), closure(std::forward<CT>(c)) {}
    ffrt_function_header_t header;
    T closure;
};

template<class T>
void exec_function_wrapper(void* t)
{
    auto f = reinterpret_cast<function<std::decay_t<T>>*>(t);
    f->closure();
}

template<class T>
void destroy_function_wrapper(void* t)
{
    auto f = reinterpret_cast<function<std::decay_t<T>>*>(t);
    f->closure = nullptr;
}

template<class T>
inline ffrt_function_header_t* create_function_wrapper(T&& func,
    ffrt_function_kind_t kind = ffrt_function_kind_general)
{
    using function_type = function<std::decay_t<T>>;
    static_assert(sizeof(function_type) <= ffrt_auto_managed_function_storage_size,
        "size of function must be less than ffrt_auto_managed_function_storage_size");

    auto p = ffrt_alloc_auto_managed_function_storage_base(kind);
    auto f =
        new (p) function_type({exec_function_wrapper<T>, destroy_function_wrapper<T>, {0}}, std::forward<T>(func));
    return reinterpret_cast<ffrt_function_header_t*>(f);
}

/**
 * @brief Submits a task without input and output dependencies.
 *
 * @param func Indicates a task executor function closure.
 * @since 10
 * @version 1.0
 */
static inline void submit(std::function<void()>&& func)
{
    return ffrt_submit_base(create_function_wrapper(std::move(func)), nullptr, nullptr, nullptr);
}

/**
 * @brief Submits a task with input dependencies only.
 *
 * @param func Indicates a task executor function closure.
 * @param in_deps Indicates a pointer to the input dependencies.
 * @since 10
 * @version 1.0
 */
static inline void submit(std::function<void()>&& func, std::initializer_list<const void*> in_deps)
{
    ffrt_deps_t in {static_cast<uint32_t>(in_deps.size()), in_deps.begin()};
    return ffrt_submit_base(create_function_wrapper(std::move(func)), &in, nullptr, nullptr);
}

/**
 * @brief Submits a task with input and output dependencies.
 *
 * @param func Indicates a task executor function closure.
 * @param in_deps Indicates a pointer to the input dependencies.
 * @param out_deps Indicates a pointer to the output dependencies.
 * @since 10
 * @version 1.0
 */
static inline void submit(std::function<void()>&& func, std::initializer_list<const void*> in_deps,
    std::initializer_list<const void*> out_deps)
{
    ffrt_deps_t in {static_cast<uint32_t>(in_deps.size()), in_deps.begin()};
    ffrt_deps_t out {static_cast<uint32_t>(out_deps.size()), out_deps.begin()};
    return ffrt_submit_base(create_function_wrapper(std::move(func)), &in, &out, nullptr);
}

/**
 * @brief Submits a task with a specified attribute and input and output dependencies.
 *
 * @param func Indicates a task executor function closure.
 * @param in_deps Indicates a pointer to the input dependencies.
 * @param out_deps Indicates a pointer to the output dependencies.
 * @param attr Indicates a task attribute.
 * @since 10
 * @version 1.0
 */
static inline void submit(std::function<void()>&& func, std::initializer_list<const void*> in_deps,
    std::initializer_list<const void*> out_deps, const task_attr& attr)
{
    ffrt_deps_t in {static_cast<uint32_t>(in_deps.size()), in_deps.begin()};
    ffrt_deps_t out {static_cast<uint32_t>(out_deps.size()), out_deps.begin()};
    return ffrt_submit_base(create_function_wrapper(std::move(func)), &in, &out, &attr);
}

/**
 * @brief Submits a task with input dependencies only.
 *
 * @param func Indicates a task executor function closure.
 * @param in_deps Indicates a pointer to the input dependencies.
 * @since 10
 * @version 1.0
 */
static inline void submit(std::function<void()>&& func, const std::vector<const void*>& in_deps)
{
    ffrt_deps_t in {static_cast<uint32_t>(in_deps.size()), in_deps.data()};
    return ffrt_submit_base(create_function_wrapper(std::move(func)), &in, nullptr, nullptr);
}

/**
 * @brief Submits a task with input and output dependencies.
 *
 * @param func Indicates a task executor function closure.
 * @param in_deps Indicates a pointer to the input dependencies.
 * @param out_deps Indicates a pointer to the output dependencies.
 * @since 10
 * @version 1.0
 */
static inline void submit(std::function<void()>&& func, const std::vector<const void*>& in_deps,
    const std::vector<const void*>& out_deps)
{
    ffrt_deps_t in {static_cast<uint32_t>(in_deps.size()), in_deps.data()};
    ffrt_deps_t out {static_cast<uint32_t>(out_deps.size()), out_deps.data()};
    return ffrt_submit_base(create_function_wrapper(std::move(func)), &in, &out, nullptr);
}

/**
 * @brief Submits a task with a specified attribute and input and output dependencies.
 *
 * @param func Indicates a task executor function closure.
 * @param in_deps Indicates a pointer to the input dependencies.
 * @param out_deps Indicates a pointer to the output dependencies.
 * @param attr Indicates a task attribute.
 * @since 10
 * @version 1.0
 */
static inline void submit(std::function<void()>&& func, const std::vector<const void*>& in_deps,
    const std::vector<const void*>& out_deps, const task_attr& attr)
{
    ffrt_deps_t in {static_cast<uint32_t>(in_deps.size()), in_deps.data()};
    ffrt_deps_t out {static_cast<uint32_t>(out_deps.size()), out_deps.data()};
    return ffrt_submit_base(create_function_wrapper(std::move(func)), &in, &out, &attr);
}

/**
 * @brief Submits a task without input and output dependencies.
 *
 * @param func Indicates a task executor function closure.
 * @since 10
 * @version 1.0
 */
static inline void submit(const std::function<void()>& func)
{
    return ffrt_submit_base(create_function_wrapper(func), nullptr, nullptr, nullptr);
}

/**
 * @brief Submits a task with input dependencies only.
 *
 * @param func Indicates a task executor function closure.
 * @param in_deps Indicates a pointer to the input dependencies.
 * @since 10
 * @version 1.0
 */
static inline void submit(const std::function<void()>& func, std::initializer_list<const void*> in_deps)
{
    ffrt_deps_t in {static_cast<uint32_t>(in_deps.size()), in_deps.begin()};
    return ffrt_submit_base(create_function_wrapper(func), &in, nullptr, nullptr);
}

/**
 * @brief Submits a task with input and output dependencies.
 *
 * @param func Indicates a task executor function closure.
 * @param in_deps Indicates a pointer to the input dependencies.
 * @param out_deps Indicates a pointer to the output dependencies.
 * @since 10
 * @version 1.0
 */
static inline void submit(const std::function<void()>& func, std::initializer_list<const void*> in_deps,
    std::initializer_list<const void*> out_deps)
{
    ffrt_deps_t in {static_cast<uint32_t>(in_deps.size()), in_deps.begin()};
    ffrt_deps_t out {static_cast<uint32_t>(out_deps.size()), out_deps.begin()};
    return ffrt_submit_base(create_function_wrapper(func), &in, &out, nullptr);
}

/**
 * @brief Submits a task with a specified attribute and input and output dependencies.
 *
 * @param func Indicates a task executor function closure.
 * @param in_deps Indicates a pointer to the input dependencies.
 * @param out_deps Indicates a pointer to the output dependencies.
 * @param attr Indicates a task attribute.
 * @since 10
 * @version 1.0
 */
static inline void submit(const std::function<void()>& func, std::initializer_list<const void*> in_deps,
    std::initializer_list<const void*> out_deps, const task_attr& attr)
{
    ffrt_deps_t in {static_cast<uint32_t>(in_deps.size()), in_deps.begin()};
    ffrt_deps_t out {static_cast<uint32_t>(out_deps.size()), out_deps.begin()};
    return ffrt_submit_base(create_function_wrapper(func), &in, &out, &attr);
}

/**
 * @brief Submits a task with input dependencies only.
 *
 * @param func Indicates a task executor function closure.
 * @param in_deps Indicates a pointer to the input dependencies.
 * @since 10
 * @version 1.0
 */
static inline void submit(const std::function<void()>& func, const std::vector<const void*>& in_deps)
{
    ffrt_deps_t in {static_cast<uint32_t>(in_deps.size()), in_deps.data()};
    return ffrt_submit_base(create_function_wrapper(func), &in, nullptr, nullptr);
}

/**
 * @brief Submits a task with input and output dependencies.
 *
 * @param func Indicates a task executor function closure.
 * @param in_deps Indicates a pointer to the input dependencies.
 * @param out_deps Indicates a pointer to the output dependencies.
 * @since 10
 * @version 1.0
 */
static inline void submit(const std::function<void()>& func, const std::vector<const void*>& in_deps,
    const std::vector<const void*>& out_deps)
{
    ffrt_deps_t in {static_cast<uint32_t>(in_deps.size()), in_deps.data()};
    ffrt_deps_t out {static_cast<uint32_t>(out_deps.size()), out_deps.data()};
    return ffrt_submit_base(create_function_wrapper(func), &in, &out, nullptr);
}

/**
 * @brief Submits a task with a specified attribute and input and output dependencies.
 *
 * @param func Indicates a task executor function closure.
 * @param in_deps Indicates a pointer to the input dependencies.
 * @param out_deps Indicates a pointer to the output dependencies.
 * @param attr Indicates a task attribute.
 * @since 10
 * @version 1.0
 */
static inline void submit(const std::function<void()>& func, const std::vector<const void*>& in_deps,
    const std::vector<const void*>& out_deps, const task_attr& attr)
{
    ffrt_deps_t in {static_cast<uint32_t>(in_deps.size()), in_deps.data()};
    ffrt_deps_t out {static_cast<uint32_t>(out_deps.size()), out_deps.data()};
    return ffrt_submit_base(create_function_wrapper(func), &in, &out, &attr);
}

/**
 * @brief Submits a task without input and output dependencies, and obtains a task handle.
 *
 * @param func Indicates a task executor function closure.
 * @return Returns a non-null task handle if the task is submitted;
           returns a null pointer otherwise.
 * @since 10
 * @version 1.0
 */
static inline task_handle submit_h(std::function<void()>&& func)
{
    return ffrt_submit_h_base(create_function_wrapper(std::move(func)), nullptr, nullptr, nullptr);
}

/**
 * @brief Submits a task with input dependencies only, and obtains a task handle.
 *
 * @param func Indicates a task executor function closure.
 * @param in_deps Indicates a pointer to the input dependencies.
 * @return Returns a non-null task handle if the task is submitted;
           returns a null pointer otherwise.
 * @since 10
 * @version 1.0
 */
static inline task_handle submit_h(std::function<void()>&& func, std::initializer_list<const void*> in_deps)
{
    ffrt_deps_t in {static_cast<uint32_t>(in_deps.size()), in_deps.begin()};
    return ffrt_submit_h_base(create_function_wrapper(std::move(func)), &in, nullptr, nullptr);
}

/**
 * @brief Submits a task with input and output dependencies, and obtains a task handle.
 *
 * @param func Indicates a task executor function closure.
 * @param in_deps Indicates a pointer to the input dependencies.
 * @param out_deps Indicates a pointer to the output dependencies.
 * @return Returns a non-null task handle if the task is submitted;
           returns a null pointer otherwise.
 * @since 10
 * @version 1.0
 */
static inline task_handle submit_h(std::function<void()>&& func, std::initializer_list<const void*> in_deps,
    std::initializer_list<const void*> out_deps)
{
    ffrt_deps_t in {static_cast<uint32_t>(in_deps.size()), in_deps.begin()};
    ffrt_deps_t out {static_cast<uint32_t>(out_deps.size()), out_deps.begin()};
    return ffrt_submit_h_base(create_function_wrapper(std::move(func)), &in, &out, nullptr);
}

/**
 * @brief Submits a task with a specified attribute and input and output dependencies, and obtains a task handle.
 *
 * @param func Indicates a task executor function closure.
 * @param in_deps Indicates a pointer to the input dependencies.
 * @param out_deps Indicates a pointer to the output dependencies.
 * @param attr Indicates a task attribute.
 * @return Returns a non-null task handle if the task is submitted;
           returns a null pointer otherwise.
 * @since 10
 * @version 1.0
 */
static inline task_handle submit_h(std::function<void()>&& func, std::initializer_list<const void*> in_deps,
    std::initializer_list<const void*> out_deps, const task_attr& attr)
{
    ffrt_deps_t in {static_cast<uint32_t>(in_deps.size()), in_deps.begin()};
    ffrt_deps_t out {static_cast<uint32_t>(out_deps.size()), out_deps.begin()};
    return ffrt_submit_h_base(create_function_wrapper(std::move(func)), &in, &out, &attr);
}

/**
 * @brief Submits a task with input dependencies only, and obtains a task handle.
 *
 * @param func Indicates a task executor function closure.
 * @param in_deps Indicates a pointer to the input dependencies.
 * @return Returns a non-null task handle if the task is submitted;
           returns a null pointer otherwise.
 * @since 10
 * @version 1.0
 */
static inline task_handle submit_h(std::function<void()>&& func, const std::vector<const void*>& in_deps)
{
    ffrt_deps_t in {static_cast<uint32_t>(in_deps.size()), in_deps.data()};
    return ffrt_submit_h_base(create_function_wrapper(std::move(func)), &in, nullptr, nullptr);
}

/**
 * @brief Submits a task with input and output dependencies, and obtains a task handle.
 *
 * @param func Indicates a task executor function closure.
 * @param in_deps Indicates a pointer to the input dependencies.
 * @param out_deps Indicates a pointer to the output dependencies.
 * @return Returns a non-null task handle if the task is submitted;
           returns a null pointer otherwise.
 * @since 10
 * @version 1.0
 */
static inline task_handle submit_h(std::function<void()>&& func, const std::vector<const void*>& in_deps,
    const std::vector<const void*>& out_deps)
{
    ffrt_deps_t in {static_cast<uint32_t>(in_deps.size()), in_deps.data()};
    ffrt_deps_t out {static_cast<uint32_t>(out_deps.size()), out_deps.data()};
    return ffrt_submit_h_base(create_function_wrapper(std::move(func)), &in, &out, nullptr);
}

/**
 * @brief Submits a task with a specified attribute and input and output dependencies, and obtains a task handle.
 *
 * @param func Indicates a task executor function closure.
 * @param in_deps Indicates a pointer to the input dependencies.
 * @param out_deps Indicates a pointer to the output dependencies.
 * @param attr Indicates a task attribute.
 * @return Returns a non-null task handle if the task is submitted;
           returns a null pointer otherwise.
 * @since 10
 * @version 1.0
 */
static inline task_handle submit_h(std::function<void()>&& func, const std::vector<const void*>& in_deps,
    const std::vector<const void*>& out_deps, const task_attr& attr)
{
    ffrt_deps_t in {static_cast<uint32_t>(in_deps.size()), in_deps.data()};
    ffrt_deps_t out {static_cast<uint32_t>(out_deps.size()), out_deps.data()};
    return ffrt_submit_h_base(create_function_wrapper(std::move(func)), &in, &out, &attr);
}

/**
 * @brief Submits a task without input and output dependencies, and obtains a task handle.
 *
 * @param func Indicates a task executor function closure.
 * @return Returns a non-null task handle if the task is submitted;
           returns a null pointer otherwise.
 * @since 10
 * @version 1.0
 */
static inline task_handle submit_h(const std::function<void()>& func)
{
    return ffrt_submit_h_base(create_function_wrapper(func), nullptr, nullptr, nullptr);
}

/**
 * @brief Submits a task with input dependencies only, and obtains a task handle.
 *
 * @param func Indicates a task executor function closure.
 * @param in_deps Indicates a pointer to the input dependencies.
 * @return Returns a non-null task handle if the task is submitted;
           returns a null pointer otherwise.
 * @since 10
 * @version 1.0
 */
static inline task_handle submit_h(const std::function<void()>& func, std::initializer_list<const void*> in_deps)
{
    ffrt_deps_t in {static_cast<uint32_t>(in_deps.size()), in_deps.begin()};
    return ffrt_submit_h_base(create_function_wrapper(func), &in, nullptr, nullptr);
}

/**
 * @brief Submits a task with input and output dependencies, and obtains a task handle.
 *
 * @param func Indicates a task executor function closure.
 * @param in_deps Indicates a pointer to the input dependencies.
 * @param out_deps Indicates a pointer to the output dependencies.
 * @return Returns a non-null task handle if the task is submitted;
           returns a null pointer otherwise.
 * @since 10
 * @version 1.0
 */
static inline task_handle submit_h(const std::function<void()>& func, std::initializer_list<const void*> in_deps,
    std::initializer_list<const void*> out_deps)
{
    ffrt_deps_t in {static_cast<uint32_t>(in_deps.size()), in_deps.begin()};
    ffrt_deps_t out {static_cast<uint32_t>(out_deps.size()), out_deps.begin()};
    return ffrt_submit_h_base(create_function_wrapper(func), &in, &out, nullptr);
}

/**
 * @brief Submits a task with a specified attribute and input and output dependencies, and obtains a task handle.
 *
 * @param func Indicates a task executor function closure.
 * @param in_deps Indicates a pointer to the input dependencies.
 * @param out_deps Indicates a pointer to the output dependencies.
 * @param attr Indicates a task attribute.
 * @return Returns a non-null task handle if the task is submitted;
           returns a null pointer otherwise.
 * @since 10
 * @version 1.0
 */
static inline task_handle submit_h(const std::function<void()>& func, std::initializer_list<const void*> in_deps,
    std::initializer_list<const void*> out_deps,  const task_attr& attr)
{
    ffrt_deps_t in {static_cast<uint32_t>(in_deps.size()), in_deps.begin()};
    ffrt_deps_t out {static_cast<uint32_t>(out_deps.size()), out_deps.begin()};
    return ffrt_submit_h_base(create_function_wrapper(func), &in, &out, &attr);
}

/**
 * @brief Submits a task with input dependencies only, and obtains a task handle.
 *
 * @param func Indicates a task executor function closure.
 * @param in_deps Indicates a pointer to the input dependencies.
 * @return Returns a non-null task handle if the task is submitted;
           returns a null pointer otherwise.
 * @since 10
 * @version 1.0
 */
static inline task_handle submit_h(const std::function<void()>& func, const std::vector<const void*>& in_deps)
{
    ffrt_deps_t in {static_cast<uint32_t>(in_deps.size()), in_deps.data()};
    return ffrt_submit_h_base(create_function_wrapper(func), &in, nullptr, nullptr);
}

/**
 * @brief Submits a task with input and output dependencies, and obtains a task handle.
 *
 * @param func Indicates a task executor function closure.
 * @param in_deps Indicates a pointer to the input dependencies.
 * @param out_deps Indicates a pointer to the output dependencies.
 * @return Returns a non-null task handle if the task is submitted;
           returns a null pointer otherwise.
 * @since 10
 * @version 1.0
 */
static inline task_handle submit_h(const std::function<void()>& func, const std::vector<const void*>& in_deps,
    const std::vector<const void*>& out_deps)
{
    ffrt_deps_t in {static_cast<uint32_t>(in_deps.size()), in_deps.data()};
    ffrt_deps_t out {static_cast<uint32_t>(out_deps.size()), out_deps.data()};
    return ffrt_submit_h_base(create_function_wrapper(func), &in, &out, nullptr);
}

/**
 * @brief Submits a task with a specified attribute and input and output dependencies, and obtains a task handle.
 *
 * @param func Indicates a task executor function closure.
 * @param in_deps Indicates a pointer to the input dependencies.
 * @param out_deps Indicates a pointer to the output dependencies.
 * @param attr Indicates a task attribute.
 * @return Returns a non-null task handle if the task is submitted;
           returns a null pointer otherwise.
 * @since 10
 * @version 1.0
 */
static inline task_handle submit_h(const std::function<void()>& func, const std::vector<const void*>& in_deps,
    const std::vector<const void*>& out_deps, const task_attr& attr)
{
    ffrt_deps_t in {static_cast<uint32_t>(in_deps.size()), in_deps.data()};
    ffrt_deps_t out {static_cast<uint32_t>(out_deps.size()), out_deps.data()};
    return ffrt_submit_h_base(create_function_wrapper(func), &in, &out, &attr);
}

/**
 * @brief Skips a task.
 *
 * @param handle Indicates a task handle.
 * @return Returns <b>0</b> if the task is skipped;
           returns <b>-1</b> otherwise.
 * @since 10
 * @version 1.0
 */
static inline int skip(task_handle &handle)
{
    return ffrt_skip(handle);
}

/**
 * @brief Waits until all submitted tasks are complete.
 *
 * @since 10
 * @version 1.0
 */
static inline void wait()
{
    ffrt_wait();
}

/**
 * @brief Waits until dependent tasks are complete.
 *
 * @param deps Indicates a pointer to the dependent tasks.
 * @since 10
 * @version 1.0
 */
static inline void wait(std::initializer_list<const void*> deps)
{
    ffrt_deps_t d {static_cast<uint32_t>(deps.size()), deps.begin()};
    ffrt_wait_deps(&d);
}

/**
 * @brief Waits until dependent tasks are complete.
 *
 * @param deps Indicates a pointer to the dependent tasks.
 * @since 10
 * @version 1.0
 */
static inline void wait(const std::vector<const void*>& deps)
{
    ffrt_deps_t d {static_cast<uint32_t>(deps.size()), deps.data()};
    ffrt_wait_deps(&d);
}

namespace this_task {
/**
 * @brief Obtains the ID of this task.
 *
 * @return Returns the task ID.
 * @since 10
 * @version 1.0
 */
static inline uint64_t get_id()
{
    return ffrt_this_task_get_id();
}
} // namespace this_task
} // namespace ffrt
#endif
