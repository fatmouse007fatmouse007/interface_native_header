/*
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef NDK_INCLUDE_EXTERNAL_NATIVE_WINDOW_H_
#define NDK_INCLUDE_EXTERNAL_NATIVE_WINDOW_H_

/**
 * @addtogroup NativeWindow
 * @{
 *
 * @brief Provides the native window capability for connection to the EGL.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @since 8
 * @version 1.0
 */

/**
 * @file external_window.h
 *
 * @brief Defines the functions for obtaining and using a native window.
 *
 * @since 8
 * @version 1.0
 */

#include <stdint.h>
#include "buffer_handle.h"

#ifdef __cplusplus
extern "C" {
#endif


/**
 * @brief Defines the <b>NativeWindow</b> structure.
 */
struct NativeWindow;

/**
 * @brief Defines the <b>NativeWindowBuffer</b> structure.
 */
struct NativeWindowBuffer;

/**
 * @brief Provides the function of accessing the <b>NativeWindow</b>.
 */
typedef struct NativeWindow OHNativeWindow;

/**
 * @brief Provides the function of accessing the <b>NativeWindowBuffer</b>.
 */
typedef struct NativeWindowBuffer OHNativeWindowBuffer;

/**
 * @brief Defines the rectangle (dirty region) where the content is to be updated in the local native window.
 */
typedef struct Region {
    /** If <b>rects</b> is a null pointer, the buffer size is the same as the size of the dirty region by default. */
    struct Rect {
        int32_t x; /**< Start X coordinate of the rectangle. */
        int32_t y; /**< Start Y coordinate of the rectangle. */
        uint32_t w; /**< Width of the rectangle. */
        uint32_t h; /**< Height of the rectangle. */
    } *rects;
    /** If <b>rectNumber</b> is <b>0</b>, the buffer size is the same as the size of the dirty region by default. */
    int32_t rectNumber;
}Region;


/**
 * @brief Enumerates the operation codes in the <b>OH_NativeWindow_NativeWindowHandleOpt</b> function.
 */
enum NativeWindowOperation {
    /**
     * Setting the geometry for the local window buffer.
     * Variable arguments in the function:
     * [Input] int32_t height and [Input] int32_t width.
     */
    SET_BUFFER_GEOMETRY,
    /**
     * Obtaining the geometry of the local window buffer.
     * Variable arguments in the function:
     * [Output] int32_t *height and [Output] int32_t *width.
     */
    GET_BUFFER_GEOMETRY,
    /**
     * Obtaining the format of the local window buffer.
     * Variable argument in the function:
     * [Output] int32_t *format.
     */
    GET_FORMAT,
    /** 
     * Setting the format for the local window buffer.
     * Variable argument in the function:
     * [Input] int32_t format.
     */
    SET_FORMAT,
    /** 
     * Obtaining the usage mode of the local window buffer.
     * Variable argument in the function:
     * [Output] int32_t *usage.
     */
    GET_USAGE,
    /** 
     * Setting the usage mode for the local window buffer.
     * Variable argument in the function:
     * [Input] int32_t usage.
     */
    SET_USAGE,
    /** 
     * Setting the stride for the local window buffer.
     * Variable argument in the function:
     * [Input] int32_t stride.
     */
    SET_STRIDE,
    /** 
     * Obtaining the stride of the local window buffer.
     * Variable argument in the function:
     * [Output] int32_t *stride.
     */
    GET_STRIDE,
    /** 
     * Setting the swap interval for the local window buffer.
     * Variable argument in the function:
     * [Input] int32_t interval.
     */
    SET_SWAP_INTERVAL,
    /** 
     * Obtaining the swap interval of the local window buffer.
     * Variable argument in the function:
     * [Output] int32_t *interval.
     */
    GET_SWAP_INTERVAL,
    /** 
     * Setting the timeout duration for requesting the local window buffer.
     * Variable argument in the function:
     * [Input] int32_t timeout.
     */
    SET_TIMEOUT,
    /** 
     * Obtaining the timeout duration for requesting the local window buffer.
     * Variable argument in the function:
     * [Output] int32_t *timeout.
     */
    GET_TIMEOUT,
    /** 
     * Setting the color gamut for the local window buffer.
     * Variable argument in the function:
     * [Input] int32_t colorGamut.
     */
    SET_COLOR_GAMUT,
    /** 
     * Obtaining the color gamut of the local window buffer.
     * Variable argument in the function:
     * [out int32_t *colorGamut].
     */
    GET_COLOR_GAMUT,
    /** 
     * Setting the transform for the local window buffer.
     * Variable argument in the function:
     * [Input] int32_t transform.
     */
    SET_TRANSFORM,
    /** 
     * Obtaining the transform of the local window buffer.
     * Variable argument in the function:
     * [Output] int32_t *transform.
     */
    GET_TRANSFORM,
    /** 
     * Setting the UI timestamp for the local window buffer.
     * Variable argument in the function:
     * [Input] uint64_t uiTimestamp.
     */
    SET_UI_TIMESTAMP,
};

/**
 * @brief Enumerates the scaling modes.
 */
typedef enum {
    /**
     * The window content cannot be updated before the buffer of the window size is received.
     */
    OH_SCALING_MODE_FREEZE = 0,
    /**
     * The buffer is scaled in two dimensions to match the window size.
     */
    OH_SCALING_MODE_SCALE_TO_WINDOW,
    /**
     * The buffer is scaled uniformly so that its smaller size can match the window size.
     */
    OH_SCALING_MODE_SCALE_CROP,
    /**
     * The window is cropped to the size of the buffer's cropping rectangle. Pixels outside the cropping rectangle are considered completely transparent.
     */
    OH_SCALING_MODE_NO_SCALE_CROP,
} OHScalingMode;

/**
 * @brief Enumerates the HDR metadata keys.
 */
typedef enum {
    OH_METAKEY_RED_PRIMARY_X = 0, /**< X coordinate of the red primary color. */
    OH_METAKEY_RED_PRIMARY_Y = 1, /**< Y coordinate of the red primary color. */
    OH_METAKEY_GREEN_PRIMARY_X = 2, /**< X coordinate of the green primary color. */
    OH_METAKEY_GREEN_PRIMARY_Y = 3, /**< Y coordinate of the green primary color. */
    OH_METAKEY_BLUE_PRIMARY_X = 4, /**< X coordinate of the blue primary color. */
    OH_METAKEY_BLUE_PRIMARY_Y = 5, /**< Y coordinate of the blue primary color. */
    OH_METAKEY_WHITE_PRIMARY_X = 6, /**< X coordinate of the white point. */
    OH_METAKEY_WHITE_PRIMARY_Y = 7, /**< Y coordinate of the white point. */
    OH_METAKEY_MAX_LUMINANCE = 8, /**< Maximum luminance. */
    OH_METAKEY_MIN_LUMINANCE = 9, /**< Minimum luminance. */
    OH_METAKEY_MAX_CONTENT_LIGHT_LEVEL = 10, /**< Maximum content light level (MaxCLL). */
    OH_METAKEY_MAX_FRAME_AVERAGE_LIGHT_LEVEL = 11, /**< Maximum frame average light level (MaxFALLL). */
    OH_METAKEY_HDR10_PLUS = 12, /**< HDR10+. */
    OH_METAKEY_HDR_VIVID = 13, /**< Vivid. */
} OHHDRMetadataKey;

/**
 * @brief Defines the HDR metadata.
 */
typedef struct {
    OHHDRMetadataKey key; /**< Key of the HDR metadata. */
    float value; /**< Value corresponding to the metadata key. */
} OHHDRMetaData;

/**
 * @brief Defines the extended data handle.
 */
typedef struct {
    int32_t fd; /**< File descriptor handle. The value <b>-1</b> indicates that the handle is not supported. */
    uint32_t reserveInts; /**< Number of reserved arrays. */
    int32_t reserve[0]; /**< Reserve array. */
} OHExtDataHandle;


/**
 * @brief Creates a <b>NativeWindow</b> instance. A new <b>NativeWindow</b> instance is created each time this function is called.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @param pSurface Indicates the pointer to a <b>ProduceSurface</b>. The type is <b>sptr<OHOS::Surface></b>.
 * @return Returns the pointer to the <b>NativeWindow</b> instance created.
 * @since 8
 * @version 1.0
 */
OHNativeWindow* OH_NativeWindow_CreateNativeWindow(void* pSurface);

/**
 * @brief Decreases the reference count of a <b>NativeWindow</b> instance by 1 and when the reference count reaches 0, destroys the instance.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @param window Indicates the pointer to a <b>NativeWindow</b> instance.
 * @since 8
 * @version 1.0
 */
void OH_NativeWindow_DestroyNativeWindow(OHNativeWindow* window);

/**
 * @brief Creates a <b>NativeWindowBuffer</b> instance. A new <b>NativeWindowBuffer</b> instance is created each time this function is called.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @param pSurfaceBuffer Indicates the pointer to a produce buffer. The type is <b>sptr<OHOS::SurfaceBuffer></b>.
 * @return Returns the pointer to the <b>NativeWindowBuffer</b> instance created.
 * @since 8
 * @version 1.0
 */
OHNativeWindowBuffer* OH_NativeWindow_CreateNativeWindowBufferFromSurfaceBuffer(void* pSurfaceBuffer);

/**
 * @brief Decreases the reference count of a <b>NativeWindowBuffer</b> instance by 1 and when the reference count reaches 0, destroys the instance.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @param buffer Indicates the pointer to a <b>NativeWindowBuffer</b> instance.
 * @since 8
 * @version 1.0
 */
void OH_NativeWindow_DestroyNativeWindowBuffer(OHNativeWindowBuffer* buffer);

/**
 * @brief Requests a <b>NativeWindowBuffer</b> through a <b>NativeWindow</b> instance for content production.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @param window Indicates the pointer to a <b>NativeWindow</b> instance.
 * @param buffer Indicates the double pointer to a <b>NativeWindowBuffer</b> instance.
 * @param fenceFd Indicates the pointer to a file descriptor handle.
 * @return Returns an error code defined in <b>GSError</b>.
 * @since 8
 * @version 1.0
 */
int32_t OH_NativeWindow_NativeWindowRequestBuffer(OHNativeWindow *window,
    OHNativeWindowBuffer **buffer, int *fenceFd);

/**
 * @brief Flushes the <b>NativeWindowBuffer</b> filled with the content to the buffer queue through a <b>NativeWindow</b> instance for content consumption.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @param window Indicates the pointer to a <b>NativeWindow</b> instance.
 * @param buffer Indicates the pointer to a <b>NativeWindowBuffer</b> instance.
 * @param fenceFd Indicates a file descriptor handle, which is used for timing synchronization.
 * @param region Indicates a dirty region where content is updated.
 * @return Returns an error code defined in <b>GSError</b>.
 * @since 8
 * @version 1.0
 */
int32_t OH_NativeWindow_NativeWindowFlushBuffer(OHNativeWindow *window, OHNativeWindowBuffer *buffer,
    int fenceFd, Region region);

/**
 * @brief Returns the <b>NativeWindowBuffer</b> to the buffer queue through a <b>NativeWindow</b> instance, without filling in any content. The <b>NativeWindowBuffer</b> can be used for another request.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @param window Indicates the pointer to a <b>NativeWindow</b> instance.
 * @param buffer Indicates the pointer to a <b>NativeWindowBuffer</b> instance.
 * @return Returns an error code defined in <b>GSError</b>.
 * @since 8
 * @version 1.0
 */
int32_t OH_NativeWindow_NativeWindowAbortBuffer(OHNativeWindow *window, OHNativeWindowBuffer *buffer);

/**
 * @brief Sets or obtains the attributes of a native window, including the width, height, and content format.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @param window Indicates the pointer to a <b>NativeWindow</b> instance.
 * @param code Indicates the operation code. For details, see {@link NativeWindowOperation}.
 * @param ... Indicates the variable argument, which must correspond to the operation code.
 * @return Returns an error code defined in <b>GSError</b>.
 * @since 8
 * @version 1.0
 */
int32_t OH_NativeWindow_NativeWindowHandleOpt(OHNativeWindow *window, int code, ...);

/**
 * @brief Obtains the pointer to a <b>BufferHandle</b> of a <b>NativeWindowBuffer</b> instance.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @param buffer Indicates the pointer to a <b>NativeWindowBuffer</b> instance.
 * @return Returns the pointer to the <b>BufferHandle</b> instance obtained.
 * @since 8
 * @version 1.0
 */
BufferHandle *OH_NativeWindow_GetBufferHandleFromNative(OHNativeWindowBuffer *buffer);

/**
 * @brief Adds the reference count of a native object.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @param obj Indicates the pointer to a <b>NativeWindow</b> or <b>NativeWindowBuffer</b> instance.
 * @return Returns an error code defined in <b>GSError</b>.
 * @since 8
 * @version 1.0
 */
int32_t OH_NativeWindow_NativeObjectReference(void *obj);

/**
 * @brief Decreases the reference count of a native object and when the reference count reaches 0, destroys this object.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @param obj Indicates the pointer to a <b>NativeWindow</b> or <b>NativeWindowBuffer</b> instance.
 * @return Returns an error code defined in <b>GSError</b>.
 * @since 8
 * @version 1.0
 */
int32_t OH_NativeWindow_NativeObjectUnreference(void *obj);

/**
 * @brief Obtains the magic ID of a native object.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @param obj Indicates the pointer to a <b>NativeWindow</b> or <b>NativeWindowBuffer</b> instance.
 * @return Returns the magic ID, which is unique for each native object.
 * @since 8
 * @version 1.0
 */
int32_t OH_NativeWindow_GetNativeObjectMagic(void *obj);

/**
 * @brief Sets the scaling mode for a native window.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @param window Indicates the pointer to a <b>NativeWindow</b> instance.
 * @param sequence Indicates the sequence of the producer buffer.
 * @param scalingMode Indicates the scaling mode to set. For details, see <b>OHScalingMode</b>.
 * @return Returns an error code defined in <b>GSError</b>.
 * @since 9
 * @version 1.0
 */
int32_t OH_NativeWindow_NativeWindowSetScalingMode(OHNativeWindow *window, uint32_t sequence,
                                                   OHScalingMode scalingMode);

/**
 * @brief Sets the metadata for a native window.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @param window Indicates the pointer to a <b>NativeWindow</b> instance.
 * @param sequence Indicates the sequence of the producer buffer.
 * @param size Indicates the size of the <b>OHHDRMetaData</b> array.
 * @param metaData Indicates the pointer to the <b>OHHDRMetaData</b> array.
 * @return Returns an error code defined in <b>GSError</b>.
 * @since 9
 * @version 1.0
 */
int32_t OH_NativeWindow_NativeWindowSetMetaData(OHNativeWindow *window, uint32_t sequence, int32_t size,
                                                const OHHDRMetaData *metaData);

/**
 * @brief Sets the metadata set for a native window.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @param window Indicates the pointer to a <b>NativeWindow</b> instance.
 * @param sequence Indicates the sequence of the producer buffer.
 * @param key Indicates a metadata key. For details, see <b>OHHDRMetadataKey</b>.
 * @param size Indicates the size of the uint8_t vector.
 * @param metaData Indicates the pointer to the uint8_t vector.
 * @return Returns an error code defined in <b>GSError</b>.
 * @since 9
 * @version 1.0
 */
int32_t OH_NativeWindow_NativeWindowSetMetaDataSet(OHNativeWindow *window, uint32_t sequence, OHHDRMetadataKey key,
                                                   int32_t size, const uint8_t *metaData);

/**
 * @brief Sets a tunnel handle for a native window.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @param window Indicates the pointer to a <b>NativeWindow</b> instance.
 * @param handle Indicates the pointer to an <b>OHExtDataHandle</b>.
 * @return Returns an error code defined in <b>GSError</b>.
 * @since 9
 * @version 1.0
 */
int32_t OH_NativeWindow_NativeWindowSetTunnelHandle(OHNativeWindow *window, const OHExtDataHandle *handle);

#ifdef __cplusplus
}
#endif

/** @} */
#endif
