/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup WLAN
 * @{
 *
 * @brief WLAN模块向上层WLAN服务提供了统一接口。
 *
 * 上层WLAN服务开发人员可根据WLAN模块提供的向上统一接口获取如下能力：建立/关闭WLAN热点，扫描/关联WLAN热点，WLAN平台芯片管理，网络数据缓冲的申请、释放、移动等操作，网络设备管理，电源管理等。
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @file IWlanCallback.idl
 *
 * @brief WLAN模块为WLAN服务提供的重启驱动、扫描结果、Netlink消息处理的回调。
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @brief WLAN模块接口的包路径。
 *
 * @since 3.2
 * @version 1.0
 */
package ohos.hdi.wlan.v1_0;

import ohos.hdi.wlan.v1_0.WlanTypes;

/**
 * @brief 定义WLAN模块的回调函数。
 *
 * 当WLAN模块发生重启，扫描热点结束，收到Netlink消息后，调用回调函数，处理对应的结果信息。
 *
 * @since 3.2
 * @version 1.0
 */
[callback] interface IWlanCallback {
    /**
     * @brief 重启WLAN驱动的结果处理回调方法。
     *
     * 当重启WLAN驱动后，调用此接口处理驱动重启后的返回结果。
     *
     * @param event 重启驱动的事件ID。
     * @param code 重启驱动后返回的结果数据。
     * @param ifName 网卡名称。
     *
     * @since 3.2
     * @version 1.0
     */
    ResetDriverResult([in] unsigned int event, [in] int code, [in] String ifName);

    /**
     * @brief 扫描结果的回调方法。
     *
     * 当扫描结束后，将通过此方法处理返回的扫描结果数据。
     *
     * @param event 扫描结果的事件ID。
     * @param scanResult 扫描结果数据。
     * @param ifName 网卡名称。
     *
     * @since 3.2
     * @version 1.0
     */
    ScanResult([in] unsigned int event, [in] struct HdfWifiScanResult scanResult, [in] String ifName);

    /**
     * @brief Netlink消息的回调方法。
     *
     * 当收到Netlink消息后，将通过此方法处理收到的消息。
     *
     * @param recvMsg 收到的Netlink消息。
     *
     * @since 3.2
     * @version 1.0
     */
    WifiNetlinkMessage([in] unsigned char[] recvMsg);
}
/** @} */
