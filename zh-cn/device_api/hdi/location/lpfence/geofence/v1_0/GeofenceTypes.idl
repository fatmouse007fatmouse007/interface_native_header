/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 /**
 * @addtogroup HdiLpfenceGeofence
 * @{
 *
 * @brief 为低功耗围栏服务提供地理围栏的API。
 *
 * 本模块接口提供添加圆形和多边形地理围栏，删除地理围栏，获取地理围栏状态信息，获取设备地理位置等功能。本模块可在AP休眠状态下持续工作。
 * 应用场景：判断用户设备是否达到某个精确地理位置区域，从而进行一些后续服务，如门禁卡的切换、定制消息的提醒等。
 *
 * @since 4.0
 * @version 1.0
 */

/**
 * @file GeofenceTypes.idl
 *
 * @brief 定义地理围栏使用的数据类型。
 *
 * @since 4.0
 * @version 1.0
 */

/**
 * @brief 地理围栏模块接口的包路径。
 *
 * @since 4.0
 */
package ohos.hdi.location.lpfence.geofence.v1_0;

/**
 * @brief 枚举可关注的地理围栏状态事件。
 *
 * @since 4.0
 */
enum GeofenceTransition {
    /** 设备在地理围栏范围内。 */
    GEOFENCE_TRANSITION_ENTERED = (1 << 0),
    /** 设备在地理围栏范围外。 */
    GEOFENCE_TRANSITION_EXITED = (1 << 1),
    /** 无法确定设备与地理围栏位置关系。 */
    GEOFENCE_TRANSITION_UNCERTAIN = (1 << 2),
    /** 设备在地理围栏内，且持续徘徊一段时间。 */
    GEOFENCE_TRANSITION_DWELL = (1 << 3),
    /** 设备在地理围栏内，且在室内。 */
    GEOFENCE_TRANSITION_INDOOR = (1 << 4),
    /** 设备在地理围栏内，且在室外。 */
    GEOFENCE_TRANSITION_OUTDOOR = (1 << 5),
};

/**
 * @brief 枚举地理围栏支持的设置项。
 *
 * @since 4.0
 */
enum GeofenceAttribute {
    /** 地理位置使用WGS-84地心坐标系。 */
    GEOFENCE_ATTRI_COORDINATE_WGS84 = 16,
};

/**
 * @brief 枚举地理围栏支持的精度模式。
 *
 * @since 4.0
 */
enum GeofenceAccuracy {
    /** 高精度模式，GNSS模块接收GPS定位信号频率为1秒1次，功耗较高。 */
    ACCURACY_FINE = 1,
    /** 中精度模式，GNSS模块接收GPS定位信号频率为60秒1次，功耗较低。 */
    ACCURACY_BALANCE = 2,
    /** 低精度模式，只使用基站定位，不依赖GNSS模块。 */
    ACCURACY_COARSE = 3,
};

/**
 * @brief 枚举移动通信技术代。
 *
 * @since 4.0
 */
enum GeofenceCellType {
    /** 第2、3、4代移动通信技术 */
    GEOFENCE_CELL_G4 = 0,
    /** 第5代移动通信技术 */
    GEOFENCE_CELL_NR = 1,
};

/**
 * @brief 枚举位置信息来源。
 *
 * @since 4.0
 */
enum GeofenceLocSource {
    /** 位置信息来源于GNSS模块 */
    GEOFENCE_GNSS = 1,
    /** 位置信息来源于Wi-Fi模块 */
    GEOFENCE_WIFI = 2,
    /** 位置信息来源于Sensor模块 */
    GEOFENCE_SENSOR = 4,
    /** 位置信息来源于基站模块 */
    GEOFENCE_CELL = 8,
    /** 位置信息来源于蓝牙模块 */
    GEOFENCE_BT = 16,
};

/**
 * @brief 定义位置坐标的数据结构。
 *
 * @since 4.0
 */
struct Point {
    /** 纬度 */
    double latitude;
    /** 经度 */
    double longitude;
};

/**
 * @brief 定义添加圆形地理围栏的数据结构。
 *
 * @since 4.0
 */
struct GeofenceCircleRequest {
    /** 地理围栏的ID号，用于标识某个地理围栏，不可重复添加相同ID号的围栏。 */
    int geofenceId;
    /** 圆形地理围栏的中心点坐标。详见{@Link Point}。 */
    struct Point point;
    /** 圆形地理围栏的半径，单位为米。 */
    double radius;
    /** 圆形地理围栏的精度。详见{@Link GeofenceAccuracy}。 */
    unsigned short accuracy;
    /** 徘徊时间，单位为毫秒，需关注{@Link GEOFENCE_TRANSITION_DWELL}事件。若设备在圆形围栏内徘徊时间达到该值，则上报{@Link GEOFENCE_TRANSITION_DWELL}事件。 */
    unsigned int loiterTimeMs;
    /** 关注的圆形围栏事件，若设备满足关注的事件则会进行上报。详见{@Link GeofenceTransition}。 */
    unsigned char monitorTransitions;
    /** 设置圆形地理围栏。详见{@Link GeofenceAttribute}。 */
    unsigned int attribute;
};

/**
 * @brief 定义添加多边形地理围栏的数据结构。
 *
 * @since 4.0
 */
struct GeofencePolygonRequest {
    /** 地理围栏的ID号，用于标识某个地理围栏，不可重复添加相同ID号的围栏。 */
    int geofenceId;
    /** 多边形地理围栏的边界坐标，仅支持凸多边形。详见{@Link Point}。 */
    struct Point[] points;
    /** 多边形地理围栏的精度。详见{@Link GeofenceAccuracy}。 */
    unsigned short accuracy;
    /** 徘徊时间，单位为毫秒，需关注{@Link GEOFENCE_TRANSITION_DWELL}事件。若设备在多边形围栏内徘徊时间达到该值，则上报{@Link GEOFENCE_TRANSITION_DWELL}事件。 */
    unsigned int loiterTimeMs;
    /** 关注的多边形围栏事件，若设备满足关注的事件则会进行上报。详见{@Link GeofenceTransition}。 */
    unsigned char monitorTransitions;
    /** 设置多边形地理围栏。详见{@Link GeofenceAttribute}。 */
    unsigned int attribute;
};

/**
 * @brief 定义添加或删除地理围栏执行结果的数据结构。
 *
 * @since 4.0
 */
struct GeofenceResult {
    /** 地理围栏的ID号。 */
    int geofenceId;
    /** 返回值。0表示操作成功，负数表示操作失败。 */
    int result;
};

/**
 * @brief 定义下发基站离线数据库的数据结构。
 *
 * @since 4.0
 */
struct OfflineDb {
    /** 当前数据包的序号，从1开始。 */
    unsigned int times;
    /** 此次下发基站离线数据库的总包数。 */
    unsigned int totalTimes;
    /** 基站离线数据库数据。 */
    unsigned char[] dbs;
};

/**
 * @brief 定义设备具体位置信息的数据结构。
 *
 * @since 4.0
 */
struct GeoLocationInfo {
    /** bit0表示经纬度是否有效，bit5表示精度是否有效。0表示无效，1表示有效。 */
    unsigned short flags;
    /** 纬度 */
    double latitude;
    /** 经度 */
    double longitude;
    /** 海拔高度，单位米 */
    double altitude;
    /** 精度，单位米 */
    float accuracy;
    /** 移动速度，单位米/秒 */
    float speed;
    /** 导向 */
    float bearing;
    /** 时间戳，单位纳秒 */
    long timeStamp;
};

/**
 * @brief 定义地理围栏使用信息的数据结构。
 *
 * @since 4.0
 */
struct GeofenceSize {
    /** 设备支持添加圆形围栏的最大个数。 */
    unsigned int circleMaxNum;
    /** 设备支持添加多边形围栏的最大个数。 */
    unsigned int polygonMaxNum;
    /** 设备当前已添加的圆形围栏个数。 */
    unsigned int circleUsedNum;
    /** 设备当前已添加的多边形围栏个数。 */
    unsigned int polygonUsedNum;
};

/**
 * @brief 定义设备驻留的基站主区信息的数据结构。
 *
 * @since 4.0
 */
struct CurrentCell {
    /** 小区号 */
    long cellId;
    /** 基站号 */
    int lac;
    /** 移动国家码 */
    unsigned short mcc;
    /** 移动网络码 */
    unsigned short mnc;
    /** 信号接收强度 */
    short rssi;
    /** 网络制式，1代表GSM，2代表WCDMA或TD-CDMA，3代表LTE，4代表5G NR。 */
    unsigned short rat;
    /** 物理小区识别码 */
    unsigned int pci;
    /** 时间戳的低32位 */
    int bootTimeLow;
    /** 时间戳的高32位 */
    int bootTimeHigh;
};

/**
 * @brief 定义设备驻留的基站邻区信息的数据结构。
 *
 * @since 4.0
 */
struct NeighborCell {
    /** 邻区ID号 */
    int physicalId;
    /** 移动国家码 */
    unsigned short mcc;
    /** 移动网络码 */
    unsigned short mnc;
    /** 信号接收强度 */
    short rssi;
    /** 网络制式 */
    unsigned short rat;
    /** 物理小区识别码 */
    unsigned int pci;
};

/**
 * @brief 定义设备驻留的基站小区信息的数据结构。
 *
 * @since 4.0
 */
struct GeofenceCellInfo {
    /** 设备驻留的基站主区信息。详见{@Link CurrentCell}。 */
    struct CurrentCell cell;
    /** 设备驻留的基站邻区信息。详见{@Link NeighborCell}。 */
    struct NeighborCell[] neighborCells;
};

/**
 * @brief 定义请求基站离线数据库数据的数据结构。
 *
 * @since 4.0
 */
struct RequestCellDb {
    /** 请求基站离线数据库的大小。 */
    int requestSize;
    /** 设备最新的位置信息。详见{@Link GeoLocationInfo}。 */
    struct GeoLocationInfo location;
    /** 设备最新的基站数据信息。详见{@Link GeofenceCellInfo}。 */
    struct GeofenceCellInfo[] cellInfo;
};
/** @} */