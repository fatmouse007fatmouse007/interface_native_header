/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 *
 * HDF is dual licensed: you can use it either under the terms of
 * the GPL, or the BSD license, at your option.
 * See the LICENSE file in the root of this repository for complete details.
 */
/**
 * @addtogroup DAC
 * @{
 *
 * @brief 提供数模转换器 (DAC) 接口。
 *
 * 该模块允许驱动程序在DAC控制器上执行操作以访问DAC通道上的设备，包括创建和销毁DAC控制器句柄和写入数据。
 *
 * @since 3.1
 */
/**
 * @file dac_if.h
 *
 * @brief 声明标准DAC接口函数。
 *
 * @since 3.1
 */	

#ifndef DAC_IF_H
#define DAC_IF_H

#include "platform_if.h"

#ifdef __cplusplus
#if __cplusplus
extern "C" {
#endif
#endif /* __cplusplus */

 /**
 * @brief DAC设备信息传输结构体
 */
struct DacIoMsg {
    /** DAC设备号 */
    uint32_t number;
    /** DAC设备通道号 */
    uint32_t channel;
};

 /**
 * @brief 获取DAC控制器的句柄。
 *
 * 您必须在访问DAC设备之前调用此函数。
 *
 * @param number 表示DAC控制器ID。
 *
 * @return DAC控制器的DevHandle指针 表示操作成功。
 * @return NULL 表示操作失败。
 *
 * @since 3.1
 */
DevHandle DacOpen(uint32_t number);

 /**
 * @brief 释放DAC控制器的句柄。
 *
 * 如果不再需要访问DAC控制器，则应调用该函数关闭其句柄，以释放未使用的内存资源。
 *
 * @param handle 表示指向DAC控制器的设备句柄的指针。
 *
 * @since 3.1 
 */
void DacClose(DevHandle handle);

 /**
 * @brief 启动DAC设备进行传输，将目标值写入指定的DAC通道。
 *
 * @param handle 表示通过DacOpen获取的DAC控制器设备句柄的指针。
 * @param channel 表示DAC传输消息所通过的通道。
 * @param val 表示设定的目标值。
 *
 * @return 0 表示操作成功。
 * @return 负值 表示操作失败。
 *
 * @since 3.1
 */
int32_t DacWrite(DevHandle handle, uint32_t channel, uint32_t val);

#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif /* __cplusplus */

#endif /* DAC_IF_H */
