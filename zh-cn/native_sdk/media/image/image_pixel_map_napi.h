/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup image
 * @{
 *
 * @brief 提供获取pixelmap的数据和信息的接口方法。
 *
 * @Syscap SystemCapability.Multimedia.Image
 * @since 8
 * @version 1.0
 */

/**
 * @file image_pixel_map_napi.h
 *
 * @brief 声明可以锁定并访问pixelmap数据的方法，声明解锁的方法。
 *
 * @since 8
 * @version 1.0
 */

#ifndef IMAGE_PIXEL_MAP_NAPI_H
#define IMAGE_PIXEL_MAP_NAPI_H
#include <stdint.h>
#include "napi/native_api.h"
#include "napi/native_node_api.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 函数方法返回值的错误码的枚举。
 *
 * @since 8
 * @version 1.0
 */
enum {
    /** 成功的结果 */
    OHOS_IMAGE_RESULT_SUCCESS = 0,
    /** 无效值 */
    OHOS_IMAGE_RESULT_BAD_PARAMETER = -1,
};

/**
 * @brief pixel 格式的枚举。
 *
 * @since 8
 * @version 1.0
 */
enum {
    /**
     * 未知的格式
     */
    OHOS_PIXEL_MAP_FORMAT_NONE = 0,
    /**
     * 32-bit RGBA. 由 R, G, B组成，包括 A 都需要占用 8 bits。存储顺序是从高位到低位。
     */
    OHOS_PIXEL_MAP_FORMAT_RGBA_8888 = 3,
    /**
     * 16-bit RGB. 仅由 R, G, B 组成。
     * 存储顺序是从高位到低位: 红色占用5 bits，绿色占用6 bits，蓝色占用5 bits。
     */
    OHOS_PIXEL_MAP_FORMAT_RGB_565 = 2,
};

/**
 * @brief 用于定义 pixel map 的相关信息。
 *
 * @since 8
 * @version 1.0
 */
struct OhosPixelMapInfo {
    /** 图片的宽, 用pixels表示 */
    uint32_t width;
    /** 图片的高, 用pixels表示 */
    uint32_t height;
    /** 每行的bytes数 */
    uint32_t rowSize;
    /** Pixel 的格式 */
    int32_t pixelFormat;
};

/**
 * @brief 用于定义 napi PixelMap 的相关信息。
 * @since 10
 * @version 2.0
 */
struct NativePixelMap;

/**
 * @brief 用于定义NativePixelMap数据类型名称。
 * @since 10
 * @version 2.0
 */
typedef struct NativePixelMap NativePixelMap;

/**
 * @brief PixelMap alpha 类型的枚举。
 *
 * @since 10
 * @version 2.0
 */
enum {
    /**
     * 未知的格式
     */
    OHOS_PIXEL_MAP_ALPHA_TYPE_UNKNOWN = 0,
    /**
     * 不透明的格式
     */
    OHOS_PIXEL_MAP_ALPHA_TYPE_OPAQUE = 1,
    /**
     * 预乘的格式
     */
    OHOS_PIXEL_MAP_ALPHA_TYPE_PREMUL = 2,
    /**
     * 预除的格式
     */
    OHOS_PIXEL_MAP_ALPHA_TYPE_UNPREMUL = 3
};

/**
 * @brief PixelMap 缩放类型的枚举。
 *
 * @since 10
 * @version 2.0
 */
enum {
    /**
     * 适应目标图片大小的格式
     */
    OHOS_PIXEL_MAP_SCALE_MODE_FIT_TARGET_SIZE = 0,
    /**
     * 以中心进行缩放的格式
     */
    OHOS_PIXEL_MAP_SCALE_MODE_CENTER_CROP = 1,
};

/**
 * @brief PixelMap 编辑类型的枚举。
 *
 * @since 10
 * @version 2.0
 */
enum {
    /**
     * 只读的格式
     */
    OHOS_PIXEL_MAP_READ_ONLY = 0,
    /**
     * 可编辑的格式
     */
    OHOS_PIXEL_MAP_EDITABLE = 1,
};

/**
 * @brief 用于定义创建 pixel map 设置选项的相关信息。
 *
 * @since 10
 * @version 2.0
 */
struct OhosPixelMapCreateOps {
    /** 图片的宽, 用pixels表示 */
    uint32_t width;
    /** 图片的高, 用pixels表示 */
    uint32_t height;
    /** 图片的格式 */
    int32_t pixelFormat;
    /** 图片的编辑类型 */
    uint32_t editable;
    /** 图片的alpha类型 */
    uint32_t alphaType;
    /** 图片的缩放类型 */
    uint32_t scaleMode;
}

/**
 * @brief 获取 <b>PixelMap</b> 的信息，并记录信息到{@link OhosPixelMapInfo}结构中。
 *
 * @param env napi的环境指针。
 * @param value 应用层的 <b>PixelMap</b> 对象。
 * @param info 用于保存信息的指针对象。 更多细节, 参看 {@link OhosPixelMapInfo}。
 * @return 如果获取并保存信息成功，则返回<b>0</b>; 如果操作失败，则返回错误码。
 * @see OhosPixelMapInfo
 * @since 8
 * @version 1.0
 */
int32_t OH_GetImageInfo(napi_env env, napi_value value, OhosPixelMapInfo *info);

/**
 * @brief 获取<b>PixelMap</b>对象数据的内存地址，并锁定该内存。
 *
 * 函数执行成功后，<b>*addrPtr</b>就是获取的待访问的内存地址。访问操作完成后，必须要使用{@link OH_UnAccessPixels}来释放锁，否则的话资源无法被释放。
 * 待解锁后，内存地址就不可以再被访问和操作。
 *
 * @param env napi的环境指针。
 * @param value 应用层的 <b>PixelMap</b> 对象。
 * @param addrPtr 用于指向的内存地址的双指针对象。
 * @see UnAccessPixels
 * @return 操作成功则返回 {@link OHOS_IMAGE_RESULT_SUCCESS}；如果操作失败，则返回错误码。
 * @since 8
 * @version 1.0
 */
int32_t OH_AccessPixels(napi_env env, napi_value value, void** addrPtr);

/**
 * @brief 释放<b>PixelMap</b>对象数据的内存锁, 用于匹配方法{@link OH_AccessPixels}。
 *
 * @param env napi的环境指针。
 * @param value 应用层的 <b>PixelMap</b> 对象。
 * @return 操作成功则返回 {@link OHOS_IMAGE_RESULT_SUCCESS}；如果操作失败，则返回错误码。
 * @see AccessPixels
 * @since 8
 * @version 1.0
 */
int32_t OH_UnAccessPixels(napi_env env, napi_value value);

/**
 * @brief 创建<b>PixelMap</b>对象。
 *
 * @param env napi的环境指针。
 * @param info pixel map 数据设置项。
 * @param buf 图片的buffer数据。
 * @param len 图片大小信息。
 * @param res 应用层的 <b>PixelMap</b> 对象的指针。
 * @return 操作成功则返回 <b>PixelMap</b> 对象；如果操作失败，则返回错误码。
 * @see CreatePixelMap
 * @since 10
 * @version 2.0
 */
int32_t OH_PixelMap_CreatePixelMap(napi_env env, OhosPixelMapCreateOps info,
    void* buf, size_t len, napi_value* res);

/**
 * @brief 根据Alpha通道的信息，来生成一个仅包含Alpha通道信息的<b>PixelMap</b>对象。
 *
 * @param env napi的环境指针。
 * @param source <b>PixelMap</b>数据设置项。
 * @param alpha alpha通道的指针。
 * @return 操作成功则返回 <b>PixelMap</b> 对象；如果操作失败，则返回错误码。
 * @see CreateAlphaPixelMap
 * @since 10
 * @version 2.0
 */
int32_t OH_PixelMap_CreateAlphaPixelMap(napi_env env, napi_value source, napi_value* alpha);

/**
 * @brief 初始化<b>PixelMap</b>对象数据。
 *
 * @param env napi的环境指针。
 * @param source <b>PixelMap</b> 数据设置项。
 * @return 操作成功则返回NativePixelMap的指针；如果操作失败，则返回错误码。
 * @see InitNativePixelMap
 * @since 10
 * @version 2.0
 */
NativePixelMap* OH_PixelMap_InitNativePixelMap(napi_env env, napi_value source);

/**
 * @brief 获取<b>PixelMap</b>对象每行字节数。
 *
 * @param native NativePixelMap的指针。
 * @param num <b>PixelMap</b>对象的每行字节数指针。
 * @return 操作成功则返回 <b>PixelMap</b> 对象每行字节数；如果操作失败，则返回错误码。
 * @see GetBytesNumberPerRow
 * @since 10
 * @version 2.0
 */
int32_t OH_PixelMap_GetBytesNumberPerRow(const NativePixelMap* native, int32_t* num);

/**
 * @brief 获取<b>PixelMap</b>对象是否可编辑的状态。
 *
 * @param native NativePixelMap的指针。
 * @param editable <b>PixelMap</b> 对象是否可编辑的指针。
 * @return 操作成功则返回编辑类型的枚举值；如果操作失败，则返回错误码。
 * @see GetIsEditable
 * @since 10
 * @version 2.0
 */
int32_t OH_PixelMap_GetIsEditable(const NativePixelMap* native, int32_t* editable);

/**
 * @brief 获取<b>PixelMap</b>对象是否支持Alpha通道。
 *
 * @param native NativePixelMap的指针。
 * @param alpha 是否支持Alpha的指针。
 * @return 操作成功则返回<b>0</b>；如果操作失败，则返回错误码。
 * @see IsSupportAlpha
 * @since 10
 * @version 2.0
 */
int32_t OH_PixelMap_IsSupportAlpha(const NativePixelMap* native, int32_t* alpha);

/**
 * @brief 设置<b>PixelMap</b>对象的Alpha通道。
 *
 * @param native NativePixelMap的指针。
 * @param alpha Alpha通道。
 * @return 操作成功则返回<b>0</b>；如果操作失败，则返回错误码。
 * @see SetAlphaAble
 * @since 10
 * @version 2.0
 */
int32_t OH_PixelMap_SetAlphaAble(const NativePixelMap* native, int32_t alpha);

/**
 * @brief 获取<b>PixelMap</b>对象像素密度。
 *
 * @param native NativePixelMap的指针。
 * @param density 像素密度指针。
 * @return 操作成功则返回像素密度；如果操作失败，则返回错误码。
 * @see GetDensity
 * @since 10
 * @version 2.0
 */
int32_t OH_PixelMap_GetDensity(const NativePixelMap* native, int32_t* density);

/**
 * @brief 设置<b>PixelMap</b>对象像素密度。
 *
 * @param native NativePixelMap的指针。
 * @param density 像素密度。
 * @return 操作成功则返回<b>0</b>；如果操作失败，则返回错误码。
 * @see GetDensity
 * @since 10
 * @version 2.0
 */
int32_t OH_PixelMap_SetDensity(const NativePixelMap* native, int32_t density);

/**
 * @brief 设置<b>PixelMap</b>对象的透明度。
 *
 * @param native NativePixelMap的指针。
 * @param opacity 透明度。
 * @return 操作成功则返回<b>0</b>；如果操作失败，则返回错误码。
 * @see SetOpacity
 * @since 10
 * @version 2.0
 */
int32_t OH_PixelMap_SetOpacity(const NativePixelMap* native, float opacity);

/**
 * @brief 设置<b>PixelMap</b>对象的缩放。
 *
 * @param native NativePixelMap的指针。
 * @param x 缩放宽度。
 * @param y 缩放高度。
 * @return 操作成功则返回<b>0</b>；如果操作失败，则返回错误码。
 * @see Scale
 * @since 10
 * @version 2.0
 */
int32_t OH_PixelMap_Scale(const NativePixelMap* native, float x, float y);

/**
 * @brief 设置<b>PixelMap</b>对象的偏移。
 *
 * @param native NativePixelMap的指针。
 * @param x 水平偏移量。
 * @param y 垂直偏移量。
 * @return 操作成功则返回<b>0</b>；如果操作失败，则返回错误码。
 * @see Translate
 * @since 10
 * @version 2.0
 */
int32_t OH_PixelMap_Translate(const NativePixelMap* native, float x, float y);

/**
 * @brief 设置<b>PixelMap</b>对象的旋转。
 *
 * @param native NativePixelMap的指针。
 * @param angle 旋转角度。
 * @return 操作成功则返回<b>0</b>；如果操作失败，则返回错误码。
 * @see Rotate
 * @since 10
 * @version 2.0
 */
int32_t OH_PixelMap_Rotate(const NativePixelMap* native, float angle);

/**
 * @brief 设置<b>PixelMap</b>对象的翻转。
 *
 * @param native NativePixelMap的指针。
 * @param x 根据水平方向x轴进行图片翻转。
 * @param y 根据垂直方向y轴进行图片翻转。
 * @return 操作成功则返回<b>0</b>；如果操作失败，则返回错误码。
 * @see Flip
 * @since 10
 * @version 2.0
 */
int32_t OH_PixelMap_Flip(const NativePixelMap* native, int32_t x, int32_t y);

/**
 * @brief 设置<b>PixelMap</b>对象的裁剪。
 *
 * @param native NativePixelMap的指针。
 * @param x 目标图片左上角的x坐标。
 * @param y 目标图片左上角的y坐标。
 * @param width 裁剪区域的宽度。
 * @param height 裁剪区域的高度。
 * @return 操作成功则返回<b>0</b>；如果操作失败，则返回错误码。
 * @see Crop
 * @since 10
 * @version 2.0
 */
int32_t OH_PixelMap_Crop(const NativePixelMap* native, int32_t x, int32_t y, int32_t width, int32_t height);

/**
 * @brief 获取<b>PixelMap</b>对象图像信息。
 *
 * @param native NativePixelMap的指针。
 * @param info 图像信息指针。
 * @return 操作成功则返回<b>0</b>；如果操作失败，则返回错误码。
 * @see OhosPixelMapInfo
 * @since 10
 * @version 2.0
 */
int32_t OH_PixelMap_GetImageInfo(const NativePixelMap* native, OhosPixelMapInfo *info);

/**
 * @brief 获取native <b>PixelMap</b> 对象数据的内存地址，并锁定该内存。
 *
 * @param native NativePixelMap的指针。
 * @param addr 用于指向的内存地址的双指针对象。
 * @see UnAccessPixels
 * @return 操作成功则返回 {@link OHOS_IMAGE_RESULT_SUCCESS}；如果操作失败，则返回错误码。
 * @since 10
 * @version 2.0
 */
int32_t OH_PixelMap_AccessPixels(const NativePixelMap* native, void** addr);

/**
 * @brief 释放native <b>PixelMap</b>对象数据的内存锁，用于匹配方法{@link OH_PixelMap_AccessPixels}。
 *
 * @param native NativePixelMap的指针。
 * @return 操作成功则返回 {@link OHOS_IMAGE_RESULT_SUCCESS}；如果操作失败，则返回错误码。
 * @see AccessPixels
 * @since 10
 * @version 2.0
 */
int32_t OH_PixelMap_UnAccessPixels(const NativePixelMap* native);

#ifdef __cplusplus
};
#endif

/** @} */
#endif // IMAGE_PIXEL_MAP_NAPI_H

