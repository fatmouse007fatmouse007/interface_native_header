/*
 * Copyright (C) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup image
 * @{
 *
 * @brief 提供image接口的访问。
 *
 * @Syscap SystemCapability.Multimedia.Image
 * @since 10
 * @version 2.0
 */

/**
 * @file image_mdk_common.h
 *
 * @brief 声明图像常用的枚举值和结构体。
 *
 * @since 10
 * @version 2.0
 */

#ifndef INTERFACES_KITS_NATIVE_INCLUDE_IMAGE_COMMON_H_
#define INTERFACES_KITS_NATIVE_INCLUDE_IMAGE_COMMON_H_
namespace OHOS {
namespace Media {
#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 可能被使用的接口返回值的枚举。
 *
 * @since 10
 * @version 2.0
 */
enum {
    /** 成功的结果 */
    OHOS_IMAGE_RESULT_SUCCESS = 0,
    /** 无效参数 */
    OHOS_IMAGE_RESULT_BAD_PARAMETER = -1,
};

/**
 * @brief 定义图像大小。
 *
 * @since 10
 * @version 2.0
 */
struct OhosImageSize {
    /** 像素中的图像宽度，用pixels表示 */
    int32_t width;
    /** 像素中的图像高度，用pixels表示 */
    int32_t height;
};

#ifdef __cplusplus
};
#endif
/** @} */
} // namespace Media
} // namespace OHOS
#endif // INTERFACES_KITS_NATIVE_INCLUDE_IMAGE_COMMON_H_
