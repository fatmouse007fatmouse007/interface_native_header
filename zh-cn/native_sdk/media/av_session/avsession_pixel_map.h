/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef OHOS_AVSESSION_PIXEL_MAP_H
#define OHOS_AVSESSION_PIXEL_MAP_H

/**
 * @addtogroup avsession
 * @{
 *
 * @brief 音视频媒体会话，提供系统内媒体的统一控制能力。
 *
 * 功能包括媒体会话，媒体会话管理，媒体会话控制。
 *
 * @syscap SystemCapability.Multimedia.AVSession.Core
 * @since 9
 * @version 1.0
 */

/**
 * @file avsession_pixel_map.h
 *
 * @brief 读取、设置图片及图片信息。
 *
 * @since 9
 * @version 1.0
 */

#include <vector>
#include "parcel.h"

#if !defined(WINDOWS_PLATFORM) and !defined(MAC_PLATFORM) and !defined(IOS_PLATFORM)
#include <malloc.h>
#endif

namespace OHOS::AVSession {
/** 初始化容器大小为160KB */
constexpr size_t DEFAULT_BUFFER_SIZE = 160 * 1024;

/**
 * @brief 读取、设置图片及图片信息。
 *
 */
class AVSessionPixelMap : public Parcelable {
public:
    AVSessionPixelMap() = default;
    ~AVSessionPixelMap()
    {
#if !defined(WINDOWS_PLATFORM) and !defined(MAC_PLATFORM) and !defined(IOS_PLATFORM)
#if defined(__BIONIC__)
        mallopt(M_PURGE, 0);
#endif
#endif
    }

    /**
     * @brief 实现图片及图片信息的序列化。
     *
     * @param data 保存序列化值的对象{@link Parcel}。
     * @return 如果序列化成功，则返回true；失败则返回false。
     * @see Unmarshalling
     * @since 9
     * @version 1.0
     */
    bool Marshalling(Parcel &data) const override;

    /**
     * @brief 实现图片及图片信息的反序列化。
     *
     * @param data 保存反序列化值的对象{@link Parcel}。
     * @return 如果反序列化成功，则返回true；失败则返回false。
     * @see Marshalling
     * @since 9
     * @version 1.0
     */
    static AVSessionPixelMap *Unmarshalling(Parcel &data);

    /**
     * @brief 获取图片数据。
     *
     * @return 返回图片数据。
     * @see SetPixelData
     * @since 9
     * @version 1.0
     */
    std::vector<uint8_t> GetPixelData() const
    {
        return data_;
    }

    /**
     * @brief 设置图片数据。
     *
     * @param data 图片数据。
     * @see GetPixelData
     * @since 9
     * @version 1.0
     */
    void SetPixelData(const std::vector<uint8_t> &data)
    {
        data_.clear();
        data_ = data;
    }

    /**
     * @brief 获取图片信息。
     *
     * @return 返回图片信息。
     * @see SetImageInfo
     * @since 9
     * @version 1.0
     */
    std::vector<uint8_t> GetImageInfo() const
    {
        return imageInfo_;
    }

    /**
     * @brief 设置图片信息。
     *
     * @param imageInfo 图片信息。
     * @see GetImageInfo
     * @since 9
     * @version 1.0
     */
    void SetImageInfo(const std::vector<uint8_t> &imageInfo)
    {
        imageInfo_.clear();
        imageInfo_ = imageInfo;
    }

private:
    /** 图片数据 */
    std::vector<uint8_t> data_ = std::vector<uint8_t>(DEFAULT_BUFFER_SIZE);
    /** 图片信息 */
    std::vector<uint8_t> imageInfo_;
};
} // namespace OHOS::AVSession
/** @} */
#endif // OHOS_AVSESSION_PIXEL_MAP_H