/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef NDK_INCLUDE_NATIVE_VSYNC_H_
#define NDK_INCLUDE_NATIVE_VSYNC_H_

/**
 * @addtogroup NativeVsync
 * @{
 *
 * @brief 提供NativeVsync功能
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeVsync
 * @since 9
 * @version 1.0
 */

/**
 * @file native_vsync.h
 *
 * @brief 定义获取和使用NativeVsync的相关函数
 *
 * @since 9
 * @version 1.0
 */

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 提供OH_NativeVSync结构体声明
 * @since 9
 */
struct OH_NativeVSync;

/**
 * @brief 提供OH_NativeVSync结构体声明
 * @since 9
 */
typedef struct OH_NativeVSync OH_NativeVSync;

/**
 * @brief VSync回调函数类型
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeVsync
 * @param timestamp VSync时间戳
 * @param data 用户自定义数据
 * @since 9
 * @version 1.0
 */
typedef void (*OH_NativeVSync_FrameCallback)(long long timestamp, void *data);

/**
 * @brief 创建一个OH_NativeVSync实例，每次调用都会产生一个新的实例
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeVsync
 * @param name 表示一个名字，与创建的OH_NativeVSync实例关联
 * @param length name的长度
 * @return 返回一个指向OH_NativeVSync实例的指针
 * @since 9
 * @version 1.0
 */
OH_NativeVSync* OH_NativeVSync_Create(const char* name, unsigned int length);

/**
 * @brief 销毁OH_NativeVSync实例
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeVsync
 * @param nativeVsync 一个指向OH_NativeVSync实例的指针
 * @since 9
 * @version 1.0
 */
void OH_NativeVSync_Destroy(OH_NativeVSync* nativeVsync);

/**
 * @brief 请求下一次vsync信号，当信号到来时，调用回调函数callback
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeVsync
 * @param nativeVsync 一个指向OH_NativeVSync实例的指针
 * @param callback 一个OH_NativeVSync_FrameCallback类型的函数指针，当下一次vsync信号到来时会被调用
 * @param data 一个指向用户自定义数据结构的指针，类型是void*
 * @return 返回值为0表示执行成功
 * @since 9
 * @version 1.0
 */
int OH_NativeVSync_RequestFrame(OH_NativeVSync* nativeVsync, OH_NativeVSync_FrameCallback callback, void* data);
#ifdef __cplusplus
}
#endif

#endif