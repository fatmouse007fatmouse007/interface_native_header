/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef RELATIONAL_STORE_H
#define RELATIONAL_STORE_H

/**
 * @addtogroup RDB
 * @{
 *
 * @brief 关系型数据库（Relational Database，RDB）是一种基于关系模型来管理数据的数据库。关系型数据库基于SQLite组件提供了一套完整的
 * 对本地数据库进行管理的机制，对外提供了一系列的增、删、改、查等接口，也可以直接运行用户输入的SQL语句来满足复杂的场景需要。
 *
 * @syscap SystemCapability.DistributedDataManager.RelationalStore.Core
 * @since 10
 */


/**
 * @file relational_store.h
 *
 * @brief 提供管理关系数据库（RDB）方法的接口。
 *
 * @since 10
 */

#include "oh_cursor.h"
#include "oh_predicates.h"
#include "oh_values_bucket.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 数据库的安全级别枚举。
 *
 * @since 10
 */
enum OH_Rdb_SecurityLevel {
    /**
     * @brief S1: 表示数据库的安全级别为低级别。
     *
     * 当数据泄露时会产生较低影响。
     */
    S1 = 1,
    /**
     * @brief S2: 表示数据库的安全级别为中级别。
     *
     * 当数据泄露时会产生较大影响。
     */
    S2,
    /**
     * @brief S3: 表示数据库的安全级别为高级别。
     *
     * 当数据泄露时会产生重大影响。
     */
    S3,
    /**
     * @brief S4: 表示数据库的安全级别为关键级别。
     *
     * 当数据泄露时会产生严重影响。
     */
    S4
};

/**
 * @brief 管理关系数据库配置。
 *
 * @since 10
 */
typedef struct {
    /** 数据库文件路径。 */
    const char *path;
    /** 指定数据库是否加密。 */
    bool isEncrypt;
    /** 设置数据库安全级别{@link OH_Rdb_SecurityLevel}。 */
    enum OH_Rdb_SecurityLevel securityLevel;
} OH_Rdb_Config;

/**
 * @brief 表示数据库类型。
 *
 * @since 10
 */
typedef struct {
    /** OH_Rdb_Store结构体的唯一标识符。 */
    int64_t id;
} OH_Rdb_Store;

/**
 * @brief 创建{@link OH_VObject}实例。
 *
 * @return 创建成功则返回一个指向{@link OH_VObject}结构体实例的指针，否则返回NULL。
 * @see OH_VObject.
 * @since 10
 */
OH_VObject *OH_Rdb_CreateValueObject(void);

/**
 * @brief 创建{@link OH_VBucket}实例。
 *
 * @return 创建成功则返回一个指向{@link OH_VBucket}结构体实例的指针，否则返回NULL。
 * @see OH_VBucket.
 * @since 10
 */
OH_VBucket *OH_Rdb_CreateValuesBucket(void);

/**
 * @brief 创建{@link OH_Predicates}实例。
 *
 * @param table 表示数据库表名。
 * @return 创建成功则返回一个指向{@link OH_Predicates}结构体实例的指针，否则返回NULL。
 * @see OH_Predicates.
 * @since 10
 */
OH_Predicates *OH_Rdb_CreatePredicates(const char *table);

/**
 * @brief 获得一个相关的{@link OH_Rdb_Store}实例，操作关系型数据库。
 *
 * @param config 表示指向{@link OH_Rdb_Config}实例的指针，与此RDB存储相关的数据库配置。
 * @param errCode 该参数是输出参数，函数执行状态写入该变量。
 * @return 创建成功则返回一个指向{@link OH_Rdb_Store}结构体实例的指针，否则返回NULL。
 * @see OH_Rdb_Config, OH_Rdb_Store.
 * @since 10
 */
OH_Rdb_Store *OH_Rdb_GetOrOpen(const OH_Rdb_Config *config, int *errCode);

/**
 * @brief 销毁{@link OH_Rdb_Store}对象，并回收该对象占用的内存。
 *
 * @param store 表示指向{@link OH_Rdb_Store}实例的指针。
 * @return 返回操作是否成功，出错时返回对应的错误码。
 * @see OH_Rdb_Store.
 * @since 10
 */
int OH_Rdb_CloseStore(OH_Rdb_Store *store);

/**
 * @brief 使用指定的数据库文件配置删除数据库。
 *
 * @param path 表示数据库路径。
 * @return 返回操作是否成功，出错时返回对应的错误码。
 * @since 10
 */
int OH_Rdb_DeleteStore(const char *path);

/**
 * @brief 向目标表中插入一行数据。
 *
 * @param store 表示指向{@link OH_Rdb_Store}实例的指针。
 * @param table 表示指定的目标表名。
 * @param valuesBucket 表示要插入到表中的数据行{@link OH_VBucket}。
 * @return 如果插入成功，返回行ID。否则返回特定的错误码。
 * @see OH_Rdb_Store, OH_VBucket.
 * @since 10
 */
int OH_Rdb_Insert(OH_Rdb_Store *store, const char *table, OH_VBucket *valuesBucket);

/**
 * @brief 根据指定的条件更新数据库中的数据。
 *
 * @param store 表示指向{@link OH_Rdb_Store}实例的指针。
 * @param valuesBucket 表示要更新到表中的数据行{@link OH_VBucket}。
 * @param predicates 表示指向{@link OH_Predicates}实例的指针，指定更新条件。
 * @return 如果更新成功，返回受影响的行数，否则返回特定的错误码。
 * @see OH_Rdb_Store, OH_Bucket, OH_Predicates.
 * @since 10
 */
int OH_Rdb_Update(OH_Rdb_Store *store, OH_VBucket *valuesBucket, OH_Predicates *predicates);

/**
 * @brief 根据指定的条件删除数据库中的数据。
 *
 * @param store 表示指向{@link OH_Rdb_Store}实例的指针。
 * @param predicates 表示指向{@link OH_Predicates}实例的指针，指定删除条件。
 * @return 如果更新成功，返回受影响的行数，否则返回特定的错误码。
 * @see OH_Rdb_Store, OH_Predicates.
 * @since 10
 */
int OH_Rdb_Delete(OH_Rdb_Store *store, OH_Predicates *predicates);

/**
 * @brief 根据指定条件查询数据库中的数据
 *
 * @param store 表示指向{@link OH_Rdb_Store}实例的指针。
 * @param predicates 表示指向{@link OH_Predicates}实例的指针，指定查询条件。
 * @param columnNames 表示要查询的列。如果值为空，则查询应用于所有列。
 * @param length 表示columnNames数组的长度。
 * @return 如果查询成功则返回一个指向{@link OH_Cursor}结构体实例的指针，否则返回NULL。
 * @see OH_Rdb_Store, OH_Predicates, OH_Cursor.
 * @since 10
 */
OH_Cursor *OH_Rdb_Query(OH_Rdb_Store *store, OH_Predicates *predicates, const char *const *columnNames, int length);

/**
 * @brief 执行无返回值的SQL语句。
 *
 * @param store 表示指向{@link OH_Rdb_Store}实例的指针。
 * @param sql 指定要执行的SQL语句。
 * @return 返回操作是否成功，出错时返回对应的错误码。
 * @see OH_Rdb_Store.
 * @since 10
 */
int OH_Rdb_Execute(OH_Rdb_Store *store, const char *sql);

/**
 * @brief 根据指定SQL语句查询数据库中的数据。
 *
 * @param store 表示指向{@link OH_Rdb_Store}实例的指针。
 * @param sql 指定要执行的SQL语句。
 * @return 如果查询成功则返回一个指向{@link OH_Cursor}结构体实例的指针，否则返回NULL。
 * @see OH_Rdb_Store.
 * @since 10
 */
OH_Cursor *OH_Rdb_ExecuteQuery(OH_Rdb_Store *store, const char *sql);

/**
 * @brief 在开始执行SQL语句之前，开始事务。
 *
 * @param store 表示指向{@link OH_Rdb_Store}实例的指针。
 * @return 返回操作是否成功，出错时返回对应的错误码。
 * @see OH_Rdb_Store.
 * @since 10
 */
int OH_Rdb_BeginTransaction(OH_Rdb_Store *store);

/**
 * @brief 回滚已经执行的SQL语句。
 *
 * @param store 表示指向{@link OH_Rdb_Store}实例的指针。
 * @return 返回操作是否成功，出错时返回对应的错误码。
 * @see OH_Rdb_Store.
 * @since 10
 */
int OH_Rdb_RollBack(OH_Rdb_Store *store);

/**
 * @brief 提交已执行的SQL语句
 *
 * @param store 表示指向{@link OH_Rdb_Store}实例的指针。
 * @return 返回操作是否成功，出错时返回对应的错误码。
 * @see OH_Rdb_Store.
 * @since 10
 */
int OH_Rdb_Commit(OH_Rdb_Store *store);

/**
 * @brief 以指定路径备份数据库。
 *
 * @param store 表示指向{@link OH_Rdb_Store}实例的指针。
 * @param databasePath 指定数据库的备份文件路径。
 * @return 返回操作是否成功，出错时返回对应的错误码。
 * @see OH_Rdb_Store.
 * @since 10
 */
int OH_Rdb_Backup(OH_Rdb_Store *store, const char *databasePath);

/**
 * @brief 从指定的数据库备份文件恢复数据库。
 *
 * @param store 表示指向{@link OH_Rdb_Store}实例的指针。
 * @param databasePath 指定数据库的备份文件路径。
 * @return 返回操作是否成功，出错时返回对应的错误码。
 * @see OH_Rdb_Store.
 * @since 10
 */
int OH_Rdb_Restore(OH_Rdb_Store *store, const char *databasePath);

/**
 * @brief 获取数据库版本。
 *
 * @param store 表示指向{@link OH_Rdb_Store}实例的指针。
 * @param version 表示版本号。
 * @return 返回操作是否成功，出错时返回对应的错误码。
 * @see OH_Rdb_Store.
 * @since 10
 */
int OH_Rdb_GetVersion(OH_Rdb_Store *store, int *version);

/**
 * @brief 设置数据库版本。
 *
 * @param store 表示指向{@link OH_Rdb_Store}实例的指针。
 * @param version 示版本号。
 * @return 返回操作是否成功，出错时返回对应的错误码。
 * @see OH_Rdb_Store.
 * @since 10
 */
int OH_Rdb_SetVersion(OH_Rdb_Store *store, int version);

#ifdef __cplusplus
};
#endif

#endif // RELATIONAL_STORE_H
