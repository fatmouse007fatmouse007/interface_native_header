/**
 * Copyright 2021 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied。
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup MindSpore
 * @{
 * 
 * @brief 提供MindSpore Lite的模型推理相关接口。
 * 
 * @Syscap SystemCapability.Ai.MindSpore
 * @since 9
 */

/**
 * @file model.h
 * 
 * @brief 提供了模型相关接口，可以用于模型创建、模型推理等。
 * 
 * @since 9
 */

#ifndef MINDSPORE_INCLUDE_C_API_MODEL_C_H
#define MINDSPORE_INCLUDE_C_API_MODEL_C_H

#include "mindspore/tensor.h"
#include "mindspore/context.h"
#include "mindspore/status.h"

#ifdef __cplusplus
extern "C"
{
#endif
/**
 * @brief 指向模型对象的指针。
 *
 * @since 9
 */
typedef void *OH_AI_ModelHandle;

/**
 * @brief 张量数组结构体，用于存储张量数组指针和张量数组长度
 *
 * @since 9
 */
typedef struct OH_AI_TensorHandleArray
{
  /** 张量数组长度 */
  size_t handle_num;
  /** 指向张量数组的指针 */
  OH_AI_TensorHandle *handle_list;
} OH_AI_TensorHandleArray;

/**
 * @brief 维度信息，最大的维度为{@link MS_MAX_SHAPE_NUM}。
 *
 * @since 9
 */
#define OH_AI_MAX_SHAPE_NUM 32
typedef struct OH_AI_ShapeInfo
{
  /** 维度数组长度 */
  size_t shape_num;
  /** 维度数组 */
  int64_t shape[OH_AI_MAX_SHAPE_NUM];
} OH_AI_ShapeInfo;

/**
 * @brief 回调函数中传入的算子信息。
 * 
 * @since 9
 */
typedef struct OH_AI_CallBackParam
{
  /** 算子名称 */
  char *node_name;
  /** 算子类型 */
  char *node_type;
} OH_AI_CallBackParam;

/**
 * @brief 回调函数指针。
 *
 * 该函数指针是用于设置{@link OH_AI_ModelPredict}函数参数中的两个回调函数。
 * 该指针指向的函数需要包含三个参数，其中inputs和outputs对应了算子的输入和输出张量，kernel_Info表示当前算子的信息。
 * 可以通过回调函数监控算子执行的情况，例如统计算子的执行时间，校验算子的正确性等等。
 * 
 * @since 9
 */
typedef bool (*OH_AI_KernelCallBack)(const OH_AI_TensorHandleArray inputs, const OH_AI_TensorHandleArray outputs,
                                      const OH_AI_CallBackParam kernel_Info);

/**
 * \brief  创建一个模型对象。
 *
 * \return 模型对象指针。
 * @since 9
 */
OH_AI_API OH_AI_ModelHandle OH_AI_ModelCreate();

/**
 * \brief  释放一个模型对象。
 *
 * \param model 模型对象指针。
 * @since 9
 */
OH_AI_API void OH_AI_ModelDestroy(OH_AI_ModelHandle *model);

/**
 * \brief  从内存缓冲区加载并编译MindSpore模型。
 *
 * 注意，同一个{@link OH_AI_ContextHandle}对象仅能传递给{@link OH_AI_ModelBuild}或者{@link OH_AI_ModelBuildFromFile}一次，
 * 如果多次调用该函数需要创建多个不同的{@link OH_AI_ContextHandle}。
 * 
 * \param model 模型对象指针。
 * \param model_data  内存中已经加载的模型数据地址。
 * \param data_size 模型数据的长度。
 * \param model_type 模型文件类型，具体见{@link OH_AI_ModelType}。
 * \param model_context 模型运行时的上下文环境，具体见 {@link OH_AI_ContextHandle}。
 * \return 枚举类型的状态码{@link OH_AI_Status}，若返回MSStatus::kMSStatusSuccess则证明创建成功。
 * @since 9
 */
OH_AI_API OH_AI_Status OH_AI_ModelBuild(OH_AI_ModelHandle model, const void *model_data, size_t data_size,
                                        OH_AI_ModelType model_type, const OH_AI_ContextHandle model_context);

/**
 * \brief  通过模型文件加载并编译MindSpore模型。
 *
 * 注意，同一个{@link OH_AI_ContextHandle}对象仅能传递给{@link OH_AI_ModelBuild}或者{@link OH_AI_ModelBuildFromFile}一次，
 * 如果多次调用该函数需要创建多个不同的{@link OH_AI_ContextHandle}。
 * 
 * \param model 模型对象指针。
 * \param model_path 模型文件路径。
 * \param model_type 模型文件类型，具体见{@link OH_AI_ModelType}。
 * \param model_context 模型运行时的上下文环境，具体见 {@link OH_AI_ContextHandle}。
 * \return 枚举类型的状态码{@link OH_AI_Status}，若返回MSStatus::kMSStatusSuccess则证明创建成功。
 * @since 9
 */
OH_AI_API OH_AI_Status OH_AI_ModelBuildFromFile(OH_AI_ModelHandle model, const char *model_path,
                                                OH_AI_ModelType model_type, const OH_AI_ContextHandle model_context);

/**
 * \brief  调整已编译模型的输入形状。
 *
 * \param model 模型对象指针。
 * \param inputs 模型输入对应的张量数组结构体。
 * \param shape_infos 输入形状信息数组，按模型输入顺序排列的由形状信息组成的数组，模型会按顺序依次调整张量形状。
 * \param shape_info_num 形状信息数组的长度。
 * \return 枚举类型的状态码{@link OH_AI_Status}，若返回MSStatus::kMSStatusSuccess则证明创建成功。
 * @since 9
 */
OH_AI_API OH_AI_Status OH_AI_ModelResize(OH_AI_ModelHandle model, const OH_AI_TensorHandleArray inputs,
                                          OH_AI_ShapeInfo *shape_infos, size_t shape_info_num);

/**
 * \brief 执行模型推理。
 *
 * \param model 模型对象指针。
 * \param inputs 模型输入对应的张量数组结构体。
 * \param outputs 模型输出对应的张量数组结构体的指针。
 * \param before 模型推理前执行的回调函数。
 * \param after 模型推理后执行的回调函数。
 * \return 枚举类型的状态码{@link OH_AI_Status}，若返回MSStatus::kMSStatusSuccess则证明创建成功。
 * @since 9
 */
OH_AI_API OH_AI_Status OH_AI_ModelPredict(OH_AI_ModelHandle model, const OH_AI_TensorHandleArray inputs,
                                          OH_AI_TensorHandleArray *outputs, const OH_AI_KernelCallBack before,
                                          const OH_AI_KernelCallBack after);

/**
 * \brief 获取模型的输入张量数组结构体。
 *
 * \param model 模型对象指针。
 * \return 模型输入对应的张量数组结构体。
 * @since 9
 */
OH_AI_API OH_AI_TensorHandleArray OH_AI_ModelGetInputs(const OH_AI_ModelHandle model);

/**
 * \brief  获取模型的输出张量数组结构体。
 *
 * \param model 模型对象指针。
 * \return 模型输出对应的张量数组结构体。
 * @since 9
 */
OH_AI_API OH_AI_TensorHandleArray OH_AI_ModelGetOutputs(const OH_AI_ModelHandle model);

/**
 * \brief  通过张量名获取模型的输入张量。
 *
 * \param model 模型对象指针。
 * \param tensor_name 张量名称。
 * \return tensor_name所对应的输入张量的张量指针，如果输入中没有该张量则返回空。
 * @since 9
 */
OH_AI_API OH_AI_TensorHandle OH_AI_ModelGetInputByTensorName(const OH_AI_ModelHandle model, const char *tensor_name);

/**
 * \brief  通过张量名获取模型的输出张量。
 *
 * \param model 模型对象指针。
 * \param tensor_name 张量名称。
 * \return tensor_name所对应的输入张量的张量指针，如果输出中没有该张量则返回空。
 * @since 9
 */
OH_AI_API OH_AI_TensorHandle OH_AI_ModelGetOutputByTensorName(const OH_AI_ModelHandle model, const char *tensor_name);

#ifdef __cplusplus
}
#endif

/** @} */
#endif // MINDSPORE_INCLUDE_C_API_MODEL_C_H
